package com.mrk.NoDimOnApps;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class NoDimOnAppsBroadcast extends BroadcastReceiver {
	public static final String LIGHTS_OFF = "com.mrk.NoDimOnApps.LIGHTS_OFF";
	public static final int LIGHT_OFF_ALARM = 69;

	public void onReceive(Context context, Intent intent) {
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			Intent service = new Intent();
			service.setComponent(new ComponentName(context.getPackageName(), NoDimOnAppsService.class.getName()));
			context.startService(service);
		}

		if (("android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) && (!intent.getBooleanExtra("android.intent.extra.REPLACING", false))
				&& (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("launch_on_new_app", true))) {
			SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(context);

			if (preference.getBoolean("launch_on_new_app", true)) {
				Intent activity = new Intent(context, NewPackageActivity.class);
				activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.setData(intent.getData());
				context.startActivity(activity);
			}
		}
	}
}
