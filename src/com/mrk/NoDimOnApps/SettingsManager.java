package com.mrk.NoDimOnApps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

public class SettingsManager {
	
	public List<ApplicationSetting> appList = new ArrayList<ApplicationSetting>();
	private Context context = null;
	private boolean isEmpty = true;	
	private boolean isLoading = false;
	
	public SettingsManager(Context context) {
		this.context = context;
	}
	
	public void writeList() {
		if (!isLoading) {
			CsvManager csv = new CsvManager(context, "preferences.csv", "w");
			
			try {
				csv.writeLine(getVersionName());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			for (int i=0; i<appList.size() ; i++) {
				if (appList.get(i).isManaged()) {
					try {
						csv.writeLine(appList.get(i).getTitle() + ";" + appList.get(i).getPackage() + ";" + 
									  appList.get(i).getBatteryScreenTimeOut() + ";" + appList.get(i).getACScreenTimeOut() + ";" +
									  appList.get(i).getProcess());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			csv.close();
		}
	}
	
	public void readList(boolean addIfNotFound) {
		boolean firstLine = true;
		boolean isReading = true;
		String ligne = null;
		String strVersionName = getVersionName();
		CsvManager csv = new CsvManager(context, "preferences.csv", "r");
		
		while (isReading) {
			try {
				ApplicationSetting app = new ApplicationSetting();
				ligne = csv.readLine();
				isReading = (ligne != null);
				if (isReading) {
					isEmpty = false;
					if (firstLine) {
						if (ligne.contains(";")) {
							strVersionName="0";
						} else {
							strVersionName = ligne;
						}
						firstLine = false;
					}
					if (ligne.contains(";")) {
						String[] line = ligne.split(";");
						
						if (strVersionName.equals("0")) {
							app.setTitle(line[0]);
							app.setPackage(line[1]);
							app.setBatteryScreenTimeOut(Integer.parseInt(line[2]));
							app.setACScreenTimeOut(Integer.parseInt(line[2]));
							app.setProcess(line[3]);						
						} else if (Float.parseFloat(strVersionName) < 3.2) {
							app.setTitle(line[0]);
							app.setPackage(line[1]);
							app.setBatteryScreenTimeOut(Integer.parseInt(line[2]));
							app.setACScreenTimeOut(Integer.parseInt(line[3]));
							app.setProcess(line[4]);
						} else {
							app.setTitle(line[0]);
							app.setPackage(line[1]);
							app.setBatteryScreenTimeOut( ( Integer.parseInt(line[2]) == -1 ? 1800 : Integer.parseInt(line[2]) ) );
							app.setACScreenTimeOut( ( Integer.parseInt(line[3]) == -1 ? 1800 : Integer.parseInt(line[3]) ) );
							app.setProcess(line[4]);
						}
						app.setManaged(true);
						
						addToList(app, addIfNotFound);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		csv.close();
	}
	
	public List<ApplicationSetting> getAppsSettingsList() {
		return appList;
	}
	
	public int addToList(ApplicationSetting appSetting, boolean addIfNotFound) {
		boolean found = false;
		for (int index=0; index<appList.size(); index++) {
			if (appList.get(index).equals(appSetting)) {
				found = true;
				appList.get(index).setBatteryScreenTimeOut(appSetting.getBatteryScreenTimeOut());
				appList.get(index).setACScreenTimeOut(appSetting.getACScreenTimeOut());
				appList.get(index).setManaged(appSetting.isManaged());
			}
		}
		if (!found && addIfNotFound) {
			appList.add(appSetting);
		}
		return appList.indexOf(appSetting);
	}
	
	public void clearList() {
		this.appList.clear();
	}
	
	public ArrayList<String> getAppsList() {
		if (!isEmpty) {
			ArrayList<String> app = new ArrayList<String>();
			for (int i=0; i<appList.size() ; i++) {
				if (!appList.get(i).getPackage().startsWith("com.google.android.apps.maps")) {
					app.add(appList.get(i).getPackage()); 
				} else {
					app.add(appList.get(i).getProcess());
				}			
			}
			return app;
		}
		return null;
	}
	
	public ArrayList<Integer> getBatteryScreenOffList() {
		if (!isEmpty) {
			ArrayList<Integer> app = new ArrayList<Integer>();
			for (int i=0; i<appList.size() ; i++) {
				app.add(appList.get(i).getBatteryScreenTimeOut());
			}
			return app;
		}
		return null;
	}
	
	public ArrayList<Integer> getACScreenOffList() {
		if (!isEmpty) {
			ArrayList<Integer> app = new ArrayList<Integer>();
			for (int i=0; i<appList.size() ; i++) {
				app.add(appList.get(i).getACScreenTimeOut());
			}
			return app;
		}
		return null;
	}
	
	public ApplicationSetting getSettings(String m_package, String m_process)  {
		if (!isEmpty) {
			for (ApplicationSetting appItem : appList) {
				if (appItem.getProcess().equalsIgnoreCase(m_process) &&
						appItem.getPackage().equalsIgnoreCase(m_package))
					return appItem;
			}
		}
		return null;
	}
	
	public ApplicationSetting getDefault() {
		for (ApplicationSetting appItem : appList) {
			if (appItem.getProcess().equalsIgnoreCase(context.getResources().getString(R.string.default_title)))
				return appItem;
		}
		String defaultString = context.getResources().getString(R.string.default_title);
		ApplicationSetting defaultSetUp = new ApplicationSetting(defaultString,defaultString,15,15,defaultString);
		defaultSetUp.setManaged(true);
		return defaultSetUp;
	}
	
	public boolean equalsDefault(ApplicationSetting appSettings) {
		ApplicationSetting def = getDefault();
		
		return 
			(def.getACScreenTimeOut() == appSettings.getACScreenTimeOut() || appSettings.getACScreenTimeOut() == 0) &&
			(def.getBatteryScreenTimeOut() == appSettings.getBatteryScreenTimeOut() || appSettings.getBatteryScreenTimeOut() == 0);
	}
	
	private String getVersionName() {
		String str;
		
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
			str = packageInfo.versionName;
		} catch (NameNotFoundException e) {
			str = "version not found";
		}
		
		return str;
	}

	public boolean isLoading() {
		return isLoading;
	}

	public void setLoading(boolean isLoading) {
		this.isLoading = isLoading;
	}
}
