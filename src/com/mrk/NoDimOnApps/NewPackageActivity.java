package com.mrk.NoDimOnApps;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class NewPackageActivity extends Activity implements OnClickListener {

	private TextView newAppName;

	private ImageView newAppIcon;

	private SettingsManager managedApps;

	private int appPositionInSettings = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.new_package_layout);

		setTitle(R.string.app_name);
		
		newAppIcon = (ImageView) findViewById(R.id.img);
		newAppName = (TextView) findViewById(R.id.titre);

		Button validate = (Button) findViewById(R.id.validate);
		Button cancel = (Button) findViewById(R.id.cancel);

		validate.setOnClickListener(this);
		cancel.setOnClickListener(this);

		final TextView batTimeoutText = (TextView) findViewById(R.id.bat_timeout_text);
		SeekBar batTimeout = (SeekBar) findViewById(R.id.on_battery);

		final TextView acTimeoutText = (TextView) findViewById(R.id.ac_timeout_text);
		SeekBar acTimeout = (SeekBar) findViewById(R.id.on_ac);

		final Context context = this;

		batTimeout.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser && appPositionInSettings != -1) {
					managedApps.getAppsSettingsList().get(appPositionInSettings)
							.setBatteryScreenTimeOut(SeekBarsHelper.getTimeoutFromProgress(progress));
					managedApps
							.getAppsSettingsList()
							.get(appPositionInSettings)
							.setManaged(
									!managedApps.equalsDefault(managedApps.getAppsSettingsList().get(
											appPositionInSettings)));

					batTimeoutText.setText(SeekBarsHelper.getDisplay(managedApps, false, managedApps
							.getAppsSettingsList().get(appPositionInSettings).isManaged(), managedApps
							.getAppsSettingsList().get(appPositionInSettings).getBatteryScreenTimeOut(), context));
				}
			}

			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});

		acTimeout.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser && appPositionInSettings != -1) {
					managedApps.getAppsSettingsList().get(appPositionInSettings)
							.setACScreenTimeOut(SeekBarsHelper.getTimeoutFromProgress(progress));
					managedApps.getAppsSettingsList().get(appPositionInSettings).setManaged(

					!managedApps.equalsDefault(managedApps.getAppsSettingsList().get(appPositionInSettings)));

					acTimeoutText.setText(SeekBarsHelper.getDisplay(managedApps, false, managedApps
							.getAppsSettingsList().get(appPositionInSettings).isManaged(), managedApps
							.getAppsSettingsList().get(appPositionInSettings).getACScreenTimeOut(), context));
				}
			}

			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});

		managedApps = new SettingsManager(this);
		managedApps.readList(true);

		Intent intent = new Intent(this, NoDimOnAppsService.class);
		stopService(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();

		String newAppPackage = getIntent().getData().getEncodedSchemeSpecificPart();

		if (newAppPackage != null) {
			final PackageManager packageManager = getApplicationContext().getPackageManager();
			ApplicationInfo appInfo;

			try {
				appInfo = packageManager.getApplicationInfo(newAppPackage, 0);
			} catch (final NameNotFoundException e) {
				appInfo = null;
			}

			if (appInfo != null) {
				newAppName.setText(packageManager.getApplicationLabel(appInfo));
				newAppIcon.setBackground(packageManager.getApplicationIcon(appInfo));

				final ApplicationSetting application = new ApplicationSetting();

				application.setTitle(packageManager.getApplicationLabel(appInfo).toString());
				application.setPackage(newAppPackage);
				application.setProcess(appInfo.className);

				Intent intent = new Intent();
				intent.addCategory(Intent.CATEGORY_LAUNCHER);
				intent.setAction(Intent.ACTION_MAIN);
				intent.setPackage(newAppPackage);
				List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);

				for (ResolveInfo rInfo : list) {
					application.setProcess(rInfo.activityInfo.name);	
				}

				appPositionInSettings = managedApps.addToList(application, true);
			}
		}
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.validate) {
			sortList();
			managedApps.writeList();
		}
		this.finish();
	}

	@Override
	protected void onDestroy() {
		Intent intent = new Intent(this, NoDimOnAppsService.class);
		startService(intent);

		super.onDestroy();
	}

	private void sortList() {
		Comparator<ApplicationSetting> appsComparator = new Comparator<ApplicationSetting>() {
			public int compare(ApplicationSetting ap1, ApplicationSetting ap2) {
				if (ap1.getTitle().equals(getResources().getString(R.string.default_title)))
					return -1;
				else if (ap2.getTitle().equals(getResources().getString(R.string.default_title)))
					return 1;
				return ap1.getTitle().compareToIgnoreCase(ap2.getTitle());
			}
		};

		Collections.sort(managedApps.getAppsSettingsList(), appsComparator);
	}
}
