package com.mrk.NoDimOnApps;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageStats;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class NoDimOnAppsService extends Service {

	private static final String TAG = "NoDimOnAppsService";
	private static final int ONGOING_NOTIFICATION_ID = 200679;

	private int timer = 0;
	private int batteryScreenTimeOut = 0;
	private boolean verbose = false;
	private boolean notified = true;
	private ActivityManager activityManager = null;
	private List<RunningTaskInfo> runningTasks = null;
	private ComponentName componentInfo = null;
	private BroadcastReceiver batteryReceiver;
	private ArrayList<String> appList = null;
	private ArrayList<Integer> batteryScreenOffTimeList = null;
	private ArrayList<Integer> acScreenOffTimeList = null;

	private Handler mHandler = new Handler();
	private String previousPackage = null;
	private int lastScreenTimeOut = 0;
	boolean plugged = false;
	boolean firstCheck = true;

	boolean preLollipop = false;
	
	@Override
	public void onCreate() {
		super.onCreate();
		// FileUtils.Debug(this, TAG, "onCreate");

		_init();
		_startService();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// FileUtils.Debug(this, TAG, "onStartCommand");
		showMessage(getResources().getString(R.string.start_service));
		return START_STICKY;
	}

	@Override
	public boolean stopService(Intent name) {
		// FileUtils.Debug(this, TAG, "stopService");
		showMessage(getResources().getString(R.string.stop_service));
		return super.stopService(name);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// FileUtils.Debug(this, TAG, "onDestroy");
		_shutdownService();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// FileUtils.Debug(this, TAG, "onUnbind");
		return super.onUnbind(intent);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// FileUtils.Debug(this, TAG, "onBind");
		return null;
	}

	@Override
	public void onRebind(Intent intent) {
		// FileUtils.Debug(this, TAG, "onRebind");
		super.onRebind(intent);
	}

	private void _startService() {
		if (notified)
			goForeground();

		mHandler.removeCallbacks(mUpdateTimeTask);
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}

	private void goForeground() {
		NotificationCompat.Builder mBuilder;
		mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.notif)
				.setContentTitle(getText(R.string.app_title)).setContentText(getText(R.string.running));
		Intent notificationIntent = new Intent(this, NoDimOnAppsActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		mBuilder.setContentIntent(pendingIntent);

		NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification notification = mBuilder.build();

		mNotifyMgr.notify(ONGOING_NOTIFICATION_ID, notification);
		startForeground(ONGOING_NOTIFICATION_ID, notification);
	}

	private void _shutdownService() {
		mHandler.removeCallbacks(mUpdateTimeTask);
		android.provider.Settings.System.putInt(getContentResolver(), SCREEN_OFF_TIMEOUT, batteryScreenTimeOut);
		unregisterReceiver(batteryReceiver);
	}

	private void _init() {
		final SettingsManager apps = new SettingsManager(this);
		SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
		activityManager = (ActivityManager) getSystemService(Service.ACTIVITY_SERVICE);
		
		timer = 3000;
		verbose = preference.getBoolean("verbose", false);
		notified = preference.getBoolean("notified_service", false);

		apps.readList(true);

		appList = new ArrayList<String>();
		batteryScreenOffTimeList = new ArrayList<Integer>();
		acScreenOffTimeList = new ArrayList<Integer>();

		appList = apps.getAppsList();
		batteryScreenOffTimeList = apps.getBatteryScreenOffList();
		acScreenOffTimeList = apps.getACScreenOffList();

		batteryReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (firstCheck) {
					firstCheck = false;
					plugged = ((intent.getIntExtra("plugged", 0) == BatteryManager.BATTERY_PLUGGED_AC) || (intent
							.getIntExtra("plugged", 0) == BatteryManager.BATTERY_PLUGGED_USB));
					if (plugged) {
						batteryScreenTimeOut = apps.getDefault().getACScreenTimeOut();
					} else {
						batteryScreenTimeOut = apps.getDefault().getBatteryScreenTimeOut();
					}
					if (batteryScreenTimeOut != -1)
						batteryScreenTimeOut *= 1000;
				} else {
					if (plugged != ((intent.getIntExtra("plugged", 0) == BatteryManager.BATTERY_PLUGGED_AC) || (intent
							.getIntExtra("plugged", 0) == BatteryManager.BATTERY_PLUGGED_USB))) {
						plugged = !plugged;
						if (plugged) {
							batteryScreenTimeOut = apps.getDefault().getACScreenTimeOut();
						} else {
							batteryScreenTimeOut = apps.getDefault().getBatteryScreenTimeOut();
						}
						if (batteryScreenTimeOut != -1)
							batteryScreenTimeOut *= 1000;
						_checkRunningApps(true);
					}
				}
			}
		};

		IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(batteryReceiver, filter);
		
		preLollipop = Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH;
	}

	private int isManagedApp(String pkg, String proc) {
		if (appList != null) {
			if (pkg.startsWith("com.google.android.apps.maps")) {
				return appList.indexOf(proc);
			} else {
				return appList.indexOf(pkg);
			}
		} else {
			return -1;
		}
	}

	private int getProcessScreenOffTime(int index) {
		int ret = -1;

		if (plugged)
			ret = acScreenOffTimeList.get(index);
		else
			ret = batteryScreenOffTimeList.get(index);

		if (ret != -1)
			ret *= 1000;

		return ret;
	}

	public String getDisplay(int timeOut) {
		String display;

		if (timeOut != -1)
			timeOut = timeOut / 1000;

		if (timeOut < 60) {
			display = String.valueOf(timeOut) + " " + getResources().getString(R.string.seconds);
		} else {
			display = String.valueOf(timeOut / 60) + " " + getResources().getString(R.string.minutes);
		}
		if (timeOut == -1) {
			display = getResources().getString(R.string.always_on);
		}
		if (timeOut == Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString(
				"timeout", "15"))) {
			display += " " + getResources().getString(R.string.default_string);
		}

		return display;
	}

	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			_checkRunningApps(false);
			mHandler.postDelayed(this, timer);
		}
	};

	private void _checkRunningApps(boolean forceCheck) {
		try {
			int currentScreenTimeOut = 0;
			int isMng = 0;
	
			String[] topPackage = getTopPackage();
			
			if ((topPackage[0] != null && !topPackage[0].equals(previousPackage) || 
					(topPackage[0].startsWith("com.google.android.apps.maps") && topPackage[1] != null 
					&& topPackage[1].length() > 0 && !topPackage[1].equals(topPackage[1]))) || forceCheck) {
				previousPackage = topPackage[0];
	
				isMng = isManagedApp(topPackage[0], topPackage[1]);
	
				if (isMng != -1) {
					currentScreenTimeOut = getProcessScreenOffTime(isMng);
					showMessage(getResources().getString(R.string.managed_process) + " ("
							+ getDisplay(getProcessScreenOffTime(isMng)) + ")");
					setScreenTimeout(currentScreenTimeOut);
				} else {
					setScreenTimeout(batteryScreenTimeOut);
				}
			}
		} catch (Exception e) {
			FileUtils.Debug(this, TAG + " _checkRunningApps", "An exception occurred while processing the check " + e.toString());
		}
	}

	@SuppressWarnings("deprecation")
	private String[] getTopPackage() {
		String[] result = new String[2];
		
		if (preLollipop) {					
			runningTasks = activityManager.getRunningTasks(1);
			componentInfo = runningTasks.get(0).topActivity;
			result[0] = componentInfo.getPackageName();
			result[1] = componentInfo.getClassName();
		} else {
			ActivityManager mActivityManager = (ActivityManager)this.getSystemService(Context.ACTIVITY_SERVICE);
			result[0] = mActivityManager.getRunningAppProcesses().get(0).processName;
		}
		
		FileUtils.Debug(this, TAG + " _checkRunningApps after", result[0] + " -- " + result[1]);
		
		return result;
	}
	
	static class RecentUseComparator implements Comparator<UsageStats> {

	    @Override
	    public int compare(UsageStats lhs, UsageStats rhs) {
	        return (lhs.getLastTimeUsed() > rhs.getLastTimeUsed())?-1:(lhs.getLastTimeUsed() == rhs.getLastTimeUsed())?0:1;
	    }
	}
	
	private void setScreenTimeout(int newTimeout) {
		try {
			if (lastScreenTimeOut != newTimeout) {
				FileUtils.Debug(this, TAG, "setScreenTimeout ; timeout " + newTimeout);
				android.provider.Settings.System.putInt(getContentResolver(), SCREEN_OFF_TIMEOUT, newTimeout);
				lastScreenTimeOut = newTimeout;
			}
		} catch (Exception e) {
			FileUtils.Debug(this, TAG, e);
		}
	}

	private void showMessage(String message) {
		if (verbose)
			Toast.makeText(getApplication(), message, Toast.LENGTH_SHORT).show();
	}

}