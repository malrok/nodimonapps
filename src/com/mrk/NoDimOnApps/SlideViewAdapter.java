package com.mrk.NoDimOnApps;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.tjerkw.slideexpandable.library.SlideExpandableListAdapter;

public class SlideViewAdapter extends SlideExpandableListAdapter {

	private Context context;
	private SettingsManager managedApps;
	
	public SlideViewAdapter(Context context, ListAdapter wrapped, int toggle_button_id, int expandable_view_id, SettingsManager managedApps) {
		super(wrapped, toggle_button_id, expandable_view_id);
		
		this.context = context;
		this.managedApps = managedApps;
	}
	
    public View getView(final int position, View convertView, ViewGroup parent) {    	
    	convertView = super.getView(position, convertView, parent);

    	TextView batTimeoutText = (TextView) convertView.findViewById(R.id.bat_timeout_text);
		SeekBar batTimeout = (SeekBar) convertView.findViewById(R.id.on_battery);
		
		TextView acTimeoutText = (TextView) convertView.findViewById(R.id.ac_timeout_text);
		SeekBar acTimeout = (SeekBar) convertView.findViewById(R.id.on_ac);
		
		batTimeoutText.setText(SeekBarsHelper.getDisplay(managedApps, position == 0, managedApps.getAppsSettingsList().get(position).isManaged(), managedApps.getAppsSettingsList().get(position).getBatteryScreenTimeOut(), context));
		batTimeout.setProgress(SeekBarsHelper.getProgressFromTimeout(managedApps.getAppsSettingsList().get(position).getBatteryScreenTimeOut()));
		
		acTimeoutText.setText(SeekBarsHelper.getDisplay(managedApps, position == 0, managedApps.getAppsSettingsList().get(position).isManaged(), managedApps.getAppsSettingsList().get(position).getACScreenTimeOut(), context));
		acTimeout.setProgress(SeekBarsHelper.getProgressFromTimeout(managedApps.getAppsSettingsList().get(position).getACScreenTimeOut()));
		
		batTimeout.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
				if (fromUser) {
					managedApps.getAppsSettingsList().get(position).setBatteryScreenTimeOut(SeekBarsHelper.getTimeoutFromProgress(progress));
					managedApps.getAppsSettingsList().get(position).setManaged(
							(position == 0 ? true :
							!managedApps.equalsDefault(managedApps.getAppsSettingsList().get(position))
							));
					
					updated();
					
				}
            }
 
            public void onStartTrackingTouch(SeekBar seekBar) {
                
            }
 
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
		
		acTimeout.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
				if (fromUser) {
					managedApps.getAppsSettingsList().get(position).setACScreenTimeOut(SeekBarsHelper.getTimeoutFromProgress(progress));
					managedApps.getAppsSettingsList().get(position).setManaged(
							(position == 0 ? true :
							!managedApps.equalsDefault(managedApps.getAppsSettingsList().get(position))
							));
					
					updated();
				}
            }
 
            public void onStartTrackingTouch(SeekBar seekBar) {
                
            }
 
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
		
		return convertView;
    }
    
    protected void updated() { }
}
