package com.mrk.NoDimOnApps;

import com.mrk.NoDimOnApps.ImageThreadLoader.ImageLoadedListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<ApplicationSetting> {

	private Context context;
	private SettingsManager managedApps;
	private LayoutInflater inflater;
	private ImageThreadLoader imageThread;

	public ListAdapter(Context context, int textViewResourceId, SettingsManager managedApps) {
		super(context, textViewResourceId);
		inflater = LayoutInflater.from(context);
		
		imageThread = new ImageThreadLoader(context);
		
		this.context = context;
		this.managedApps = managedApps;
	}
	
	@Override
	public int getCount() {
		return managedApps.getAppsSettingsList().size();
	}
	
	@Override
	public ApplicationSetting getItem(int position) {
	    return managedApps.getAppsSettingsList().get(position);
	}
	
	@Override
	public long getItemId(int position) {
	    return position;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_layout, parent, false);
			
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.titre);
			holder.timeout = (TextView) convertView.findViewById(R.id.timeout);
			holder.image = (ImageView) convertView.findViewById(R.id.img);
			holder.reset = (Button) convertView.findViewById(R.id.reset);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.title.setText(managedApps.getAppsSettingsList().get(position).getTitle());
		holder.timeout.setText(
				getDisplay(
						position,
						managedApps.getAppsSettingsList().get(position).isManaged(),
						managedApps.getAppsSettingsList().get(position).getBatteryScreenTimeOut()));
		holder.timeout.setTextColor(Color.GRAY);

		imageThread.loadImage(managedApps.getAppsSettingsList().get(position).getPackage(), new ImageLoadedListener() {
			
			@Override
			public void imageLoaded(Drawable imageDrawable) {
				holder.image.setImageDrawable(imageDrawable);
			}
		});
		
		if (position == 0 || !managedApps.getAppsSettingsList().get(position).isManaged())
			holder.reset.setVisibility(View.GONE);
		else {
			holder.reset.setVisibility(View.VISIBLE);
			holder.reset.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View view) {
					managedApps.getAppsSettingsList().get(position).setACScreenTimeOut(-2);
					managedApps.getAppsSettingsList().get(position).setBatteryScreenTimeOut(-2);
					managedApps.getAppsSettingsList().get(position).setManaged(false);
					updated();
				}
				
			});
		}
		
		return convertView;
	}
	protected void updated() { }
	
	private class ViewHolder {
    	TextView title, timeout;
    	ImageView image;
    	Button reset;
    }
	
	private String getDisplay(int position, boolean isManaged, int timeOut) {
		String display;
		
		if (isManaged) {
			if (position != 0 && timeOut == managedApps.getDefault().getBatteryScreenTimeOut())
				display = context.getResources().getString(R.string.default_string);
			else if (timeOut == -1) display = context.getResources().getString(R.string.always_on);
			else if (timeOut == -2 || timeOut == 0) display = context.getResources().getString(R.string.default_string);
			else if (timeOut < 60) {
				display = String.valueOf(timeOut) + " " + context.getResources().getString(R.string.seconds);
			} else {
				display = String.valueOf(timeOut / 60) + " " + context.getResources().getString(R.string.minutes);
			}
		} else {
			display = context.getResources().getString(R.string.default_string);
		}
		
		return display;
	}

}
